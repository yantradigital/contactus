<?php @ob_start();
@session_start();

/*
Plugin Name: Contact Us
Plugin URI: http://www.vantageprojects.biz
Description: This is Contact Us plugin
Author: Rinku Kamboj
Version: 100.0
Author URI: http://www.vantageprojects.biz
*/


//*************** Admin function ***************



function contactusform()
{

	 global $wpdb,$signature, $sitelogo;
	 //print_r($wpdb);
	 $prefix=$wpdb->base_prefix;
	 $error=array();
	 if(isset($_POST['submitcontact']))
	 {
		$contactname=$_POST['contactname']; 
		if(trim($contactname)=='')
		{
			array_push($error,'Please enter your name.');
		}
		$emailaddress=$_POST['emailaddress']; 
		if(trim($emailaddress)=='')
		{
			array_push($error,'Please enter your emailaddress.');
		}
		$telephonenumber=$_POST['telephonenumber']; 
		if(trim($telephonenumber)=='')
		{
			array_push($error,'Please enter your telephone number.');
		}
		$help=$_POST['help'];
		$natureofenquiry=$_POST['natureofenquiry']; 
		if(trim($natureofenquiry)=='')
		{
			array_push($error,'Please enter your enquiry.');
		}
		if(count($error)>0)
		{
			for($i=0;$i<count($error);$i++)
			{
				$da.='<span class="error">'.$error[$i].'</span><br />';
			}
			$_SESSION['message2']=$da;
		}
		if(count($error)<=0)
		{
			$time=time();
			$sql="INSERT INTO `".$prefix."contactus` (`name`,`emailaddress`,`telephonenumber`, `natureofenquiry`, `submitteddate`) VALUES ('$contactname', '$emailaddress', '$telephonenumber', '$natureofenquiry', '$time')";
			$result = $wpdb->query( $sql );
			if($result==1)
			{
				$to      = $help;	
				$subject = 'Contact Us.';	
				$from = $emailaddress;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$fromname=get_option('blogname');
				$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\nFrom: $fromname <$from>\r\nReply-To: $from";
				$message=$sitelogo.'<div style="clear:both;margin-top:10px;"></div>';
				$message.="The following user has contact you. Detail has been given below:<br /><br />";
				$message.="Name: ".$contactname."<br />";
				$message.="Email: ".$emailaddress."<br />";
				$message.="Telephone Number: ".$telephonenumber."<br />";
				$message.="Nature Of Enquiry: ".$natureofenquiry."<br />";
				$message.='<div style="clear:both;margin-top:20px;"></div>'.$signature.'<div style="clear:both;margin-top:10px;"></div>';
				
				wp_mail($to, $subject, $message, $headers);
				
				$_SESSION['message']='Thank you for your message, one of our team will contact you within the next 24 hours.';
				echo"<script>window.location='';</script>";
			}
			
		}
	} 

		$data='';
		
		?>
        <script type="text/javascript">
			jQuery(document).ready(function(){
				
				
				jQuery.validator.addMethod("alpha", function(value, element) {
				return  /^[a-zA-Z() ]+$/.test(value);
				},"Only Characters Allowed.");
				jQuery("#contactusform").validate();
				jQuery('.required').css('display','block');
				
			});
			
			</script>
        <?php  $data.='<form name="contactusform" id="contactusform" method="post" action="" enctype="multipart/form-data">';
				
				$data.='<div class="contact_form fl">For an enquiry or further information, please fill in the form below
and we will do our best to respond to you as soon as possible.';
					$data.='<div class="from_main_contact">';
					if(isset($_SESSION['message2']) && $_SESSION['message2']!=''){$data.='<div class="status" style="width:320px;">'.$_SESSION['message2'].'</div>';$_SESSION['message2']='';}
					  $data.='<div class="detailname">Name :<span class="error">*</span></div>';
					  $data.='<div class="fields">';
						$data.='<input type="text" class="feidls_input required alpha" name="contactname" />';
					  $data.='</div>';
					$data.='</div>';
					$data.='<div class="from_main_contact">
					  <div class="detailname">Email Address :<span class="error">*</span></div>
					  <div class="fields">
						<input type="text" class="feidls_input required email" name="emailaddress" />
					  </div>
					</div>
					<div class="from_main_contact">
					  <div class="detailname">Telephone Number:<span class="error">*</span></div>
					  <div class="fields">
						<input type="text" class="feidls_input required" name="telephonenumber" />
					  </div>
					</div>
					<div class="from_main_contact">
					  <div class="detailname">Nature of Enquiry :<span class="error">*</span></div>
					  <div class="fields">
						<select name="help" class="feidls_input required">
							<option value="rinku.vantage@gmail.com">Subscriptions</option>
							<option value="rinku.vantage@gmail.com">Advertising</option>
							<option value="technicalsupport@merchadiseholdings.com">Technical Support</option>
							<option value="rinku.vantage@gmail.com">Recruitment / Jobs</option>
							<option value="accounts@merchandiseholdings.com">Accounts Department</option>
							<option value="rinku.vantage@gmail.com">Other</option>
						</select>
					  </div>
					</div>
					<div class="from_main_contact">
					  <div class="detailname">Enquiry :<span class="error">*</span></div>
					  <div class="fields">
						<textarea class="required" rows="5" cols="35" name="natureofenquiry"></textarea>
					  </div>
					</div>
					<div class="submitbtn">
					  <input type="submit" class="submit" value="Submit" name="submitcontact" />
					</div>';
				
          $data.='</div>';
		$data.='</form>';
		
		return $data;
}
add_shortcode('contactusform', 'contactusform');

function googlemap($attr)
{
?>
	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    
<?php
$data='<div id="map" style="width: 380px; left:45px; height: 175px; float:right;"></div>';
$address=$attr['address'];
$url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
	$url = str_replace('&amp;','&',$url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    $xml = curl_exec ($ch);
    curl_close ($ch); 
	$output= json_decode($xml);

	$lat = $output->results[0]->geometry->location->lat;
	$long = $output->results[0]->geometry->location->lng;
	
	$locations = "[
      ['".$address."', ".$lat.", ".$long.", 1]
    ]";
	$data.="
	<script type='text/javascript'>
    
	var locations = ".$locations.";

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(".$lat.", ".$long."),
	  mapTypeId: 'terrain'
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		//icon: image,
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>";
	
	return $data;
}
add_shortcode('googlemap', 'googlemap');

if(!isset($_REQUEST['step']) || ($_REQUEST['step']=='step1') )
{
	function step1() {
		include('listusers.php');
	}
}
if(isset($_REQUEST['step']) && ($_REQUEST['step']=='deletecontact') )
{
	function deletecontact() {
		include('deletecontact.php');
	}
	
	function conatct_actions2() {
		add_options_page("Contact Us", "Contactus", 1, "Contactus", "deletecontact");
	}
	add_action('admin_menu', 'conatct_actions2');
}
if(isset($_REQUEST['step']) && ($_REQUEST['step']=='replycontact') )
{
	function replycontact() {
		include('replycontact.php');
	}
	
	function conatct_actions3() {
		add_options_page("Contact Us", "Contactus", 1, "Contactus", "replycontact");
	}
	add_action('admin_menu', 'conatct_actions3');
}
function web_admin_actions() {
	add_options_page("Contact Us", "Contactus", 1, "Contactus", "step1");
}

if(!isset($_REQUEST['step']) || ($_REQUEST['step']=='step1') )
{
	add_action('admin_menu', 'web_admin_actions');
}
//add_shortcode('contact_us', 'cc');
	
	
?>
